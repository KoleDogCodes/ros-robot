from adafruit_rplidar import RPLidar
import math
import time
import threading
import serial

PORT = '/dev/ttyUSB0'
lidar = RPLidar(None, PORT)
callback = None

def start_lidar():
    thread = threading.Thread(target=start_scan)
    thread.start()

def power_off():
    lidar.stop()
    lidar.stop_motor()
    lidar.disconnect()

def validate_scan_data(data):
    null_sum = 0

    for i in range(360):
        if i in data.keys():
            pass
        else:
            if i == 0:
                null_sum = -1
                break
            else:
                null_sum += 1

    return null_sum

def start_scan():
    global lidar, callback

    scan_data = {}

    if callback == None:
        print("LIDAR > Please subscribe to the lidar output stream via .set_lidar_callback()")

    try:
        lidar.clear_input()
        print(lidar.info)

        for scan in lidar.iter_scans():
            start = time.time()

            #Get lidar data from scan
            for (_, angle, distance) in scan:
                if (distance/1000) >= 12:
                    distance = 12
                scan_data[min(359, math.floor(angle))] = round(distance / 1000, 2)

            #Validate lidar scan data
            if (validate_scan_data(scan_data) == -1):
                print("LIDAR > Invalid scan detected.")
                continue

            if validate_scan_data(scan_data) < 30:

                #Call callback function
                callback(scan_data)
                end = time.time()

                #Print elasped time
                print("Time elasped:", round(end - start, 2), "| Intergrity:", validate_scan_data(scan_data))

    except (Exception, KeyboardInterrupt, serial.SerialException) as exc:
        power_off()
        print(exc)

def set_lidar_callback(fnc):
    global callback
    callback = fnc
