from gpiozero import Robot
import time
import threading
#import LSM303 as compass

#Local Variables
robot = Robot(right=(26,19), left=(6,13))
target_heading = None
heading = None
dir = None

#Constants
TURN_SPEED = 0.8

def set_motor_speed(left_speed, right_speed):
    global dir, target_heading

    #Robot should stop
    if left_speed == 0 and right_speed == 0:
        robot.forward(0)
        dir = "stop"
        target_heading = None
        return

    #Robot should go backwards
    if left_speed < 0 and right_speed < 0:
        robot.backward(abs(left_speed))

    #Robot should go forwards
    if left_speed > 0 and right_speed > 0:
        robot.forward(left_speed)
        dir = "forward"

    #Robot should go left
    if left_speed == 0 and right_speed > 0:
        robot.left(right_speed)

    #Robot should go right
    if left_speed > 0 and right_speed == 0:
        robot.right(left_speed)

def stop():
    global dir, target_heading

    #Reset variables that keep track of robot's state
    target_heading = None
    dir = "stop"

    set_motor_speed(0, 0)
    robot.stop()

def set_target_heading(angle):
    global target_heading
    target_heading = angle

def set_current_heading(angle):
    global heading
    heading = angle

def get_target_angle():
    global target_heading
    return target_heading

def set_turn_correct_speed(spd):
    global TURN_SPEED
    TURN_SPEED = spd

def init_correction_loop():
    global target_heading, heading, dir

    #Keep robot straight when driving
    while (True):
        if target_heading == None and dir != "forward":
            continue

        if heading == None:
            continue

        if heading >= target_heading - 3 and heading <= target_heading + 3:
            print("TARGET: ", target_heading, " | CURRENT: ", heading, end='\r')
            set_motor_speed(1, 1)

        elif heading < target_heading - 3:
            print("TARGET: ", target_heading, " | CURRENT: ", heading, " | ", "TURN LEFT", end='\r')
            set_motor_speed(TURN_SPEED, 0)

        elif heading > target_heading + 3:
            print("TARGET: ", target_heading, " | CURRENT: ", heading, " | ", "TURN RIGHT", end='\r')
            set_motor_speed(0, TURN_SPEED)

#control_thread = threading.Thread(target=init_correction_loop)
#control_thread.start()
