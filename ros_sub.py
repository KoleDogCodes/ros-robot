from lib import LSM303 as compass
import roslibpy
import time
import threading

ip = '192.168.43.59'
client = roslibpy.Ros(host=ip, port=9090)
client.run()

#Publishers
pub_heading = roslibpy.Topic(client, '/robot/heading', 'std_msgs/Int16', throttle_rate=10)
pub_scan = roslibpy.Topic(client, '/robot/scan', 'std_msgs/String', throttle_rate=50)

pub_heading.advertise()
pub_scan.advertise()

def heading_publisher():
    start = time.time()

    while True:
        if (time.time() - start) >= 0.1:
            heading = compass.get_heading()
            heading_msg = roslibpy.Message({'data': heading})
            pub_heading.publish(heading_msg)
            start = time.time()

#Spawn thread for frequent publishers
heading_thread = threading.Thread(target=heading_publisher)
heading_thread.start()

#Terminate connection if program is terminated
try:
	while True:
	    pass

except KeyboardInterrupt:
	client.terminate()
